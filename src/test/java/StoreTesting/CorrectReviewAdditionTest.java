package StoreTesting;

import Pages.HomePage;
import Pages.ItemDetailsPage;
import Pages.WomenCategoryPage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class CorrectReviewAdditionTest {
    WebDriver driver;
    String URL = "https://atid.store/";

    @BeforeMethod
    public void setup() {
        System.setProperty("webdriver.chrome.driver", "C:/users/mzmuda/Desktop/chromedriver.exe");
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.get(URL);

        new HomePage(driver)
                .goToWomensCategoryPage();
    }

    @Test
    public void addition() {
        new WomenCategoryPage(driver)
                .selectItemToReview();
        new ItemDetailsPage(driver)
                .chooseReviewTab()
                .chooseNumberOfStars()
                .assertNumberOfStars()
                .writeDescriptionOfItem()
                .addNameToReview()
                .assertNameField()
                .addEmailToReview()
                .assertEmailField()
                .submitReview();
//                .assertReview();
//        Assert.assertTrue(review.isEnabled());
    }

    @AfterMethod
    public void closure() {
        driver.quit();
    }
}

