package Zadania;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;

public class EdgeDriverBase {
    protected WebDriver driver;
    protected String URL = "https://programautomatycy.pl/test-page/";

    @BeforeTest
    public void setup() {
        System.setProperty("webdriver.edge.driver", "C:/users/mzmuda/Desktop/msedgedriver.exe");
        driver = new EdgeDriver();
        driver.manage().window().maximize();
        driver.get(URL);
    }

    @AfterTest
    public void closure() {
        driver.quit();
    }
}
