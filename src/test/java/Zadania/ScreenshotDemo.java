package Zadania;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.concurrent.TimeUnit;

public class ScreenshotDemo {
    WebDriver driver;

    @BeforeTest
    public void setup() {
        System.setProperty("webdriver.chrome.driver", "c:/Users/mzmuda/Desktop/chromedriver.exe");
        driver = new ChromeDriver();

        //Set implicit wait of 3 seconds
        driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
    }

    @Test
    //Tests google calculator
    public void googleCalculator() throws IOException {
        //Tests google calculator
        driver.get("http://www.google.com");

        driver.findElement(By.cssSelector("#W0wltc")).click();
        //Write 2+2 in google textbox
        WebElement googleTextBox = driver.findElement(By.name("q"));
        googleTextBox.sendKeys("2+2");
        //Hit enter
        googleTextBox.sendKeys(Keys.ENTER);
        String result = driver.findElement(By.id("cwos")).getText();
        //Intentionaly checking for wrong calculation of 2+2=5 in order to take screenshot for failing test
        Assert.assertEquals(result, "4");
    }

    @AfterMethod
    public void takeScreenShotOnFailure(ITestResult testResult) throws IOException {
        if (testResult.getStatus() == ITestResult.FAILURE) {
            System.out.println(testResult.getStatus());
            File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
            FileUtils.copyFile(scrFile, new File("errorScreenshots\\" + testResult.getName() + "-" + Arrays.toString(testResult.getFactoryParameters()) + ".jpg"));
        }
    }
}
