package Zadania;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.Test;

public class Zadanie10 extends ChromeDriverBase {
    //2.Przejdź do strony https://programautomatycy.pl/test-page/
//3.Zlokalizuj pole tekstowe “Tytuł książki” na formularzu testowym 2 przy użyciu identyfikatora (id) i przypisz do zmiennej bookTitle
//4.Wyślij tekst do zlokalizowanego pola tekstowego w postaci dowolnego tytułu książki
//5.Zlokalizuj pole tekstowe “Tytuł filmu” na formularzu testowym 2 przy użyciu nazwy elementu (name) i przypisz do zmiennej movieTitle
//6.Wyślij tekst do zlokalizowanego pola tekstowego w postaci dowolnej nazwy filmu
//7.Zweryfikuj na stronie, czy poprawnie został wysłany tekst do pól tekstowych
    String BookTittle = "Harry Potter and the Goblet of Fire";
    String FilmTittle = "Game of Thrones";


    @Test
    public void completingFormNumberTwo() {
        WebElement bookTitle = driver.findElement(By.id("book-text"));
        bookTitle.clear();
        bookTitle.sendKeys(BookTittle);
        WebElement movieTitle = driver.findElement(By.name("your-movie"));
        movieTitle.clear();
        movieTitle.sendKeys(FilmTittle);
        String TitleToAssert = bookTitle.getAttribute("value");
        Assert.assertEquals(TitleToAssert, BookTittle);
        String MovieToAssert = movieTitle.getAttribute("value");
        Assert.assertEquals(MovieToAssert, FilmTittle);
    }
}
