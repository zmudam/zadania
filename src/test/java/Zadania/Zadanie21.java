package Zadania;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;

public class Zadanie21 extends ChromeDriverBase {
    /*1.Otwórz i zmaksymalizuj przeglądarkę Chrome
2.Przejdź do strony https://programautomatycy.pl/test-page/
3.Zlokalizuj pole tekstowe w sekcji “Jakiś tekst” na formularzu testowym 2, dowolnym sposobem.
4.Sprawdź, czy zlokalizowany element istnieje na stronie za pomocą asercji
5.Zlokalizuj cały element wielokrotnego wyboru w sekcji “Wybierz kolor” na formularzu testowym 2, przy użyciu klasy Select
6.Zaznacz opcję “Biały” za pomocą wartości elementu (value)
7.Wykonaj asercję tak, aby zwróciła błąd w konsoli
8.Zrób screena z błędem
9.Zamknij przeglądarkę oraz sesję*/


    @Test
    public void screenshotCreating() {
        WebElement textField = driver.findElement(By.id("text-text"));
        Assert.assertTrue(textField.isDisplayed());
        WebElement colorField = driver.findElement(By.className("wpcf7-form-control"));
        WebElement whiteColour = driver.findElement(By.cssSelector("[value=Biały]"));
        WebElement cookieBtn = driver.findElement(By.xpath("//*[@id=\"cn-accept-cookie\"]"));
        cookieBtn.click();
        Actions act = new Actions(driver);
        act.moveToElement(whiteColour).build().perform();
        whiteColour.click();
        Assert.assertTrue(whiteColour.getAttribute("value").contains("Zielony"));
    }

    @AfterMethod
    public void takeScreenShotOnFailure(ITestResult testResult) throws IOException {
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        if (testResult.getStatus() == ITestResult.FAILURE) {
            System.out.println(testResult.getStatus());
            File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
            FileUtils.copyFile(scrFile, new File("errorScreenshots\\" + testResult.getName() + "-" + Arrays.toString(testResult.getFactoryParameters()) + ".jpg"));
        }
    }
}
