package Zadania;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.Test;

public class Zadanie3 extends ChromeDriverBase {
/*1.Otwórz i zmaksymalizuj przeglądarkę Chrome
2.Przejdź do strony https://programautomatycy.pl/test-page/
3.Zlokalizuj pole tekstowe “Tytuł książki” na formularzu testowym 2 przy użyciu identyfikatora (id) i przypisz do zmiennej bookTitle
4.Zamknij przeglądarkę oraz sesję*/

    //    WebDriver driver;
//    String URL = "https://programautomatycy.pl/test-page/";
    String BookTittle = "Harry Potter and the Goblet of Fire";
    @Test
    public void putBookTittle() {
        WebElement bookTittle = driver.findElement(By.id("book-text"));
        bookTittle.clear();
        bookTittle.sendKeys(BookTittle);
        String value = bookTittle.getAttribute("value");
        Assert.assertEquals(value, BookTittle);
    }

}
