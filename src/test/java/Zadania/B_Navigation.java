package Zadania;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class B_Navigation {
    WebDriver driver;
    String URL = "https://www.deloitte.com";

    @BeforeTest
    public void setup() {
        System.setProperty("webdriver.chrome.driver", "C:/users/mzmuda/Desktop/chromedriver.exe");
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.get(URL);
    }

    @Test
    public void navigation() {
        driver.get("https://www.google.com/");
        driver.navigate().back();
        driver.navigate().forward();
        driver.navigate().refresh();
    }

    @AfterTest
    public void closure(){
        driver.quit();
    }


}
