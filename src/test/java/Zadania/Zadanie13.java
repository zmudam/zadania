package Zadania;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

public class Zadanie13 extends EdgeDriverBase {
/*1.Otwórz przeglądarkę Microsoft Edge
2.Przejdź do strony https://programautomatycy.pl/test-page/
3.Zlokalizuj opcję o wartości “Chomik” w sekcji “Wybierz zwierzę” na formularzu testowym 2, dowolnym sposobem
4.Pobierz i wydrukuj z wyszukanego elementu tekst “Chomik”
5.Zamknij przeglądarkę oraz sesję*/

    @Test
    public void browserSettings() {
        WebElement hamsterBtn = driver.findElement(By.cssSelector("[value='Chomik']"));
        hamsterBtn.click();
        String hamsterPrint = hamsterBtn.getAttribute("value");
        System.out.println(hamsterPrint);
    }
}
