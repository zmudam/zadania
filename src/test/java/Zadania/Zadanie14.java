package Zadania;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

public class Zadanie14 extends EdgeDriverBase {
/*1.Otwórz i zmaksymalizuj przeglądarkę Microsoft Edge
2.Przejdź do strony https://programautomatycy.pl/test-page/
3.Zlokalizuj pole tekstowe “Tytuł filmu” na formularzu testowym 2, dowolnym sposobem
4.Sprawdź, czy wyszukane pole jest edytowalne
5.Zamknij przeglądarkę oraz sesję*/

    @Test
    public void isFilmEditable() {
        WebElement filmField = driver.findElement(By.id("movie-text"));
        Object isDisabled = filmField.getAttribute("disabled");
        Assert.assertTrue("Film field is not editable", isDisabled == null);
    }


}
