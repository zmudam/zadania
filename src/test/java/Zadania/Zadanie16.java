package Zadania;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.Test;

public class Zadanie16 extends ChromeDriverBase {
    /*1.Otwórz i zmaksymalizuj przeglądarkę Chrome
2.Przejdź do strony https://programautomatycy.pl/test-page/
3.Zlokalizuj radio button liczby 3 w sekcji “Wybierz liczbę” na formularzu testowym 2, dowolnym sposobem.
4.Kliknij na zlokalizowany element
5.Sprawdź, czy kliknięty element jest zaznaczony
6.Zlokalizuj opcję wyboru koloru zielonego w sekcji “Wybierz kolor ” na formularzu testowym 2, dowolnym sposobem.
7.Kliknij na zlokalizowany element
8.Sprawdź, czy kliknięty element jest zaznaczony
9.Zamknij przeglądarkę oraz sesję */

    @Test
    public void areButtonsChecked() {
        WebElement cookieBtn = driver.findElement(By.xpath("//*[@id=\"cn-accept-cookie\"]"));
        cookieBtn.click();
        WebElement number3 = driver.findElement(By.xpath("//*[@id=\"number-radio\"]/span[3]/input"));
        number3.click();
        Assert.assertTrue(number3.isSelected());
        WebElement colorField = driver.findElement(By.className("wpcf7-form-control"));
        WebElement greenColor = driver.findElement(By.cssSelector("[value=Zielony]"));
        greenColor.click();
        Assert.assertTrue(greenColor.getAttribute("checked").equals("true"));
    }
}

