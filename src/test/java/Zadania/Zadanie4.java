package Zadania;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.Test;

public class Zadanie4 extends ChromeDriverBase {
    //    WebDriver driver;
//    String URL = "https://programautomatycy.pl/test-page/";
    String FilmTittle = "Game of Thrones";
//
//    @BeforeTest
//    public void setup() {
//        System.setProperty("webdriver.chrome.driver", "C:/users/mzmuda/Desktop/chromedriver.exe");
//        driver = new ChromeDriver();
//        driver.manage().window().maximize();
//        driver.get(URL);
//    }

    @Test
    public void putFilmTittle() {
        WebElement movieTitle = driver.findElement(By.name("your-movie"));
        movieTitle.clear();
        movieTitle.sendKeys(FilmTittle);
        String value = movieTitle.getAttribute("value");
        Assert.assertEquals(value, FilmTittle);
    }

//    @AfterTest
//    public void closure(){
//        driver.quit();
//    }
}
