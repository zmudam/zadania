package Zadania;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.Test;

public class Zadanie20 extends ChromeDriverBase {
/*1.Otwórz i zmaksymalizuj przeglądarkę Chrome
2.Przejdź do strony https://programautomatycy.pl/test-page/
 3.Zlokalizuj pole tekstowe w sekcji “Jakiś tekst” na formularzu testowym 2, dowolnym sposobem.
4.Sprawdź, czy zlokalizowany element istnieje na stronie za pomocą asercji
5.Zlokalizuj cały element wielokrotnego wyboru w sekcji “Wybierz kolor” na formularzu testowym 2, przy użyciu klasy Select
6.Zaznacz opcję “Biały” za pomocą wartości elementu (value)
7.Sprawdź za pomocą asercji, czy element został zaznaczony
8.Zlokalizuj radio button liczby 5 w sekcji “Wybierz liczbę” na formularzu testowym 2, dowolnym sposobem
9.Kliknij na zlokalizowany element
10.Sprawdź za pomocą asercji, czy element został zaznaczony
11.Zlokalizuj cały element wielokrotnego wyboru w sekcji “Jak Ci idzie nauka Selenium” na formularzu testowym 2
12.Zaznacz opcję "Dobrze"
13.Sprawdź za pomocą asercji, czy element został zaznaczony
14.Zamknij przeglądarkę oraz sesję*/
@Test
public void assertTextField() {
    WebElement textField =driver.findElement(By.id("text-text"));
    Assert.assertTrue(textField.isDisplayed());
    WebElement colorField = driver.findElement(By.className("wpcf7-form-control"));
    WebElement whiteColour = driver.findElement(By.cssSelector("[value=Biały]"));
    whiteColour.click();
    Assert.assertTrue(whiteColour.getAttribute("checked").equals("true"));
    WebElement number5 = driver.findElement(By.xpath("//*[@id=\"number-radio\"]/span[5]/input"));
    number5.click();
    Assert.assertTrue(number5.isSelected());
    WebElement goodCheckbox = driver.findElement(By.xpath("//*[@id=\"good-checkbox\"]/span/input"));
    WebElement cookieBtn = driver.findElement(By.xpath("//*[@id=\"cn-accept-cookie\"]"));
    cookieBtn.click();
/*    Actions act= new Actions(driver);
    act.moveToElement(checkbox).build().perform();*/
    goodCheckbox.click();
    Assert.assertTrue(goodCheckbox.isSelected());
}
}
