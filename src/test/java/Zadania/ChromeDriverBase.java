package Zadania;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;

public class ChromeDriverBase {
    protected WebDriver driver;
    protected String URL = "https://programautomatycy.pl/test-page/";

    @BeforeTest
    public void setup() {
        System.setProperty("webdriver.chrome.driver", "C:/users/mzmuda/Desktop/chromedriver.exe");
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.get(URL);
    }

    @AfterTest
    public void closure() {
        driver.quit();
    }

}
