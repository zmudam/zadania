package Zadania;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

public class Zadanie7 extends ChromeDriverBase {
/*1.Otwórz i zmaksymalizuj przeglądarkę Chrome
2.Przejdź do strony https://programautomatycy.pl/test-page/
3.Zlokalizuj pole wyboru w sekcji “Wybierz kolor” na formularzu testowym 2, przy użyciu CSS selektora za pomocą pełnej nazwy atrybutu klasy i przypisz do zmiennej coloursFullClassName.
4.Zlokalizuj pole wyboru w sekcji “Wybierz kolor” na formularzu testowym 2, przy użyciu CSS selektora za pomocą nazwy klasy i przypisz do zmiennej coloursClassName.
5.Zlokalizuj pole wyboru w sekcji “Wybierz kolor” na formularzu testowym 2, przy użyciu CSS selektora za pomocą id i przypisz do zmiennej coloursId.
6. Zamknij przeglądarkę oraz sesję
Wskazówka: Podczas lokalizacji elementu na stronie za pomocą DevTools, najedź kursorem na krawędź pola wyboru, aby wybrać cały obiekt*/

    @Test
    public void colorSelect() {
        WebElement coloursClassName = driver.findElement(By.cssSelector(".colour"));
        WebElement coloursFullClassName = driver.findElement(By.cssSelector("[name='colour[]']"));
        WebElement coloursId = driver.findElement(By.cssSelector("#colour-select-multiple"));
    }
}
