package Zadania;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

public class Zadanie11 extends EdgeDriverBase {
    /*1.Otwórz i zmaksymalizuj przeglądarkę Microsoft Edge
    2.Przejdź do strony https://programautomatycy.pl/test-page/
    3.Zlokalizuj radio button liczby 3 w sekcji “Wybierz liczbę” na formularzu testowym 2, przy użyciu xPath dowolnym sposobem i przypisz do zmiennej number3.
    4.Kliknij na zlokalizowany element
    5.Zlokalizuj opcję wyboru koloru zielonego w sekcji “Wybierz kolor ” na formularzu testowym 2, przy użyciu xPath dowolnym sposobem i przypisz do zmiennej greenColour.
    6.Kliknij na zlokalizowany element
    Zweryfikuj, czy kliknięte elementy są wybrane na stronie*/

    @Test
    public void select() {
        WebElement number3 = driver.findElement(By.xpath("//*[@id=\"number-radio\"]/span[3]/input"));
        WebElement cookieBtn = driver.findElement(By.cssSelector("#cn-close-notice"));
        cookieBtn.click();
        number3.click();
        WebElement greenColour = driver.findElement(By.xpath("//*[@id=\"colour-select-multiple\"]/option[3]"));
        greenColour.click();


    }
}
