package Zadania;

import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.edge.EdgeDriver;

public class A_Drivers {
    /**
     * Uruchom poniższy test. Nazwa metody powinna pojawić się w górnym pasku w polu rozwijanym"
     * W odpowiedzi otrzymasz błąd, ponieważ nie masz wpisanej ścieżki do drivera
     * Wybierz w polu rozwijanym opcję "Edit Configurations..."
     * W odpowiednim wpisz ścieżkę do ChromeDrivera"
     */
    @Before
    public void setup() {
        System.setProperty("webdriver.chrome.driver", "C:/users/mzmuda/Desktop/chromedriver.exe");
        System.setProperty("webdriver.edge.driver", "C:/users/mzmuda/Desktop/msedgedriver.exe");
    }

    @Test
    public void chromeDriverOnPath() {
        // Poniższa linia utworzy nową instancję sterownika ChromeDriver.
        WebDriver driver = new ChromeDriver();
        // Poniższa linia zamnknie przeglądarkę oraz sterownik.
        driver.quit();
    }


    @Test
    public void chromeDriverUsingSystemProperty() {
        //Podmień ścieżkę sterownika w poniższej linii bazując na przykładzie
//        System.setProperty("webdriver.chrome.driver", "C:/users/mzmuda/Desktop/chromedriver.exe");
        WebDriver driver = new ChromeDriver();
        driver.quit();
    }

    /**
     * Zachowanie innych przeglądarek będzie identyczne
     * Wykonaj te same czynności dla poniżej przeglądarki
     */

    @Test
    public void edgeMsedgeDriverOnPath() {
        WebDriver driver = new EdgeDriver();
        driver.quit();
    }

    @Test
    public void edgeMsedgeDriverUsingSystemProperty() {
//        System.setProperty("webdriver.edge.driver", "C:/users/mzmuda/Desktop/msedgedriver.exe");
        WebDriver driver = new EdgeDriver();
        driver.quit();
    }
}
