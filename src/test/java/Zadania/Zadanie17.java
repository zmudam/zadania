package Zadania;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.Test;

public class Zadanie17 extends ChromeDriverBase {
    /*1.Otwórz i zmaksymalizuj przeglądarkę Chrome
2.Przejdź do strony https://programautomatycy.pl/test-page/
3.Zlokalizuj pole tekstowe w sekcji “Jakiś tekst” na formularzu testowym 2, dowolnym sposobem.
4.Sprawdź, czy zlokalizowany element istnieje na stronie
5.Zamknij przeglądarkę oraz sesję*/

    @Test
    public void isTextExist() {
        WebElement somethingText = driver.findElement(By.id("description-text"));
        somethingText.click();
        Assert.assertTrue(somethingText.isDisplayed());
    }
}
