package Zadania;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.Test;

public class Zadanie19 extends EdgeDriverBase {
    /*1.Otwórz i zmaksymalizuj przeglądarkę Microsoft Edge
2.Przejdź do strony https://programautomatycy.pl/test-page/
3.Zlokalizuj cały element wielokrotnego wyboru w sekcji “Wybierz kolor” na formularzu testowym 2, przy użyciu klasy Select
4.Zaznacz opcję “Biały” za pomocą wartości elementu (value)
5.Sprawdź czy element został zaznaczony
6.Zamknij przeglądarkę oraz sesję*/

    @Test
    public void isWhiteColorChecked() {
        WebElement whiteColour = driver.findElement(By.cssSelector("[value=Biały]"));
        whiteColour.click();
        Assert.assertTrue(whiteColour.getAttribute("checked").equals("true"));
    }

}
