package Zadania;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

public class Zadanie8 extends EdgeDriverBase {
/*1.Otwórz i zmaksymalizuj przeglądarkę Microsoft Edge
2.Przejdź do strony https://programautomatycy.pl/test-page/
3.Zlokalizuj radio button liczby 3 w sekcji “Wybierz liczbę” na formularzu testowym 2, przy użyciu xPath dowolnym sposobem i przypisz do zmiennej number3.
4.Zlokalizuj opcję wyboru koloru zielonego w sekcji “Wybierz kolor ” na formularzu testowym 2, przy użyciu xPath dowolnym sposobem i przypisz do zmiennej greenColour.
5.Zamknij przeglądarkę oraz sesję*/

    @Test
    public void optionsPick() {
        WebElement number3 = driver.findElement(By.xpath("//*[@id=\"number-radio\"]/span[3]/input"));
        WebElement greenColour = driver.findElement(By.xpath("//*[@id=\"colour-select-multiple\"]/option[3]"));
    }
}
