package Pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class WomenCategoryPage {
    protected WebDriver driver;

    @FindBy(xpath = "//*[@id=\"main\"]/div/ul/li[3]/div[2]/a/h2")
    private WebElement trousersLabel;

    public WomenCategoryPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public ItemDetailsPage selectItemToReview() {
        Actions action = new Actions(driver);
        action.moveToElement(trousersLabel).build().perform();
        trousersLabel.click();
        return new ItemDetailsPage(driver);
    }

    public WebElement getTrousersLabel() {
        return trousersLabel;
    }

    public void setTrousersLabel(WebElement trousersLabel) {
        this.trousersLabel = trousersLabel;
    }
}
