package Pages;

import com.github.javafaker.Faker;
import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class ItemDetailsPage {
    protected WebDriver driver;
    private String name;
    private String email;

    public ItemDetailsPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    @FindBy(css = "#tab-title-reviews")
    private WebElement reviewBtn;

    @FindBy(css = ".star-5")
    private WebElement starsBtn;

    @FindBy(css = "#comment")
    private WebElement commentField;

    @FindBy(css = "#author")
    private WebElement nameField;

    @FindBy(css = "#submit")
    private WebElement submitBtn;

    @FindBy(css = "#email")
    private WebElement emailField;

    @FindBy(css = "..comment_container")
    private WebElement review;

    public ItemDetailsPage chooseReviewTab() {
        reviewBtn.click();
        return this;
    }

    public ItemDetailsPage chooseNumberOfStars() {
        starsBtn.click();
        return this;
    }

    public ItemDetailsPage assertNumberOfStars() {
        Assert.assertTrue("Incorrect number od stars", starsBtn.getAttribute("class").contains("active"));
        return this;
    }

    public ItemDetailsPage writeDescriptionOfItem() {
        commentField.clear();
        commentField.sendKeys("good quality trousers");
        return this;
    }

    public ItemDetailsPage addNameToReview() {
        nameField.clear();
        Faker fakerName = new Faker();
        name = fakerName.name().firstName();
        nameField.sendKeys(name);
        return this;
    }

    public ItemDetailsPage assertNameField() {
        Assert.assertTrue(nameField.getAttribute("value").equals(name));
        return this;
    }

    public ItemDetailsPage addEmailToReview() {
        Faker fakerEmail = new Faker();
        email = fakerEmail.name().username();
        emailField.sendKeys(email + "gmail.com");
        ;
        return this;
    }

    public ItemDetailsPage assertEmailField() {
        Assert.assertTrue(emailField.getAttribute("value").equals(email + "gmail.com"));
        return this;
    }

    public ItemDetailsPage submitReview() {
        submitBtn.click();
        return this;
    }

    public ItemDetailsPage assertReview() {
        Assert.assertTrue(review.isEnabled());
        return this;
    }

    public WebElement getReviewBtn() {
        return reviewBtn;
    }

    public void setReviewBtn(WebElement reviewBtn) {
        this.reviewBtn = reviewBtn;
    }

    public WebElement getStarsBtn() {
        return starsBtn;
    }

    public void setStarsBtn(WebElement starsBtn) {
        this.starsBtn = starsBtn;
    }

    public WebElement getCommentField() {
        return commentField;
    }

    public void setCommentField(WebElement commentField) {
        this.commentField = commentField;
    }

    public WebElement getNameField() {
        return nameField;
    }

    public void setNameField(WebElement nameField) {
        this.nameField = nameField;
    }

    public WebElement getSubmitBtn() {
        return submitBtn;
    }

    public void setSubmitBtn(WebElement submitBtn) {
        this.submitBtn = submitBtn;
    }

    public WebElement getEmailField() {
        return emailField;
    }

    public void setEmailField(WebElement emailField) {
        this.emailField = emailField;
    }

    public WebElement getReview() {
        return review;
    }

    public void setReview(WebElement review) {
        this.review = review;
    }
}
