package Pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class HomePage {
    protected WebDriver driver;

    @FindBy(css = "#menu-item-267")
    public WebElement womenBtn;

    public WebElement getWomenBtn() {
        return womenBtn;
    }

    public void setWomenBtn(WebElement womenBtn) {
        this.womenBtn = womenBtn;
    }

    public HomePage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public WomenCategoryPage goToWomensCategoryPage() {
        womenBtn.click();
        return new WomenCategoryPage(driver);
    }


}
